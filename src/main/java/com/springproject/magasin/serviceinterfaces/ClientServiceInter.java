package com.springproject.magasin.serviceinterfaces;

import com.springproject.magasin.models.Client;
import com.springproject.magasin.models.Commande;

import java.util.List;
import java.util.Set;

public interface ClientServiceInter {
    List<Client> getClients();

    Client saveClient(Client client);

    Client getClientById(Long id);

    Client updateClient(Client client);

    String deleteClient(Long id);

    Set<Commande> getCommandesClient(Long id);
}
