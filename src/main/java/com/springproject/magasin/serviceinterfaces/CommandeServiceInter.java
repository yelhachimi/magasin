package com.springproject.magasin.serviceinterfaces;

import com.springproject.magasin.models.Commande;

import java.util.List;

public interface CommandeServiceInter {

    List<Commande> getCommandes();

    Commande saveCommande(Commande commande);

    Commande getCommandeById(Long id);

    Commande updateCommande(Commande commande);

    String deleteCommande(Long id);
}
