package com.springproject.magasin.serviceinterfaces;

import com.springproject.magasin.models.Categorie;

import java.util.List;

public interface CategorieServiceInter {
    List<Categorie> getCategories();

    Categorie saveCategorie(Categorie categorie);

    Categorie getCategorieById(Long id);

    Categorie updateCategorie(Categorie categorie);

    String deleteCategorie(Long id);
}
