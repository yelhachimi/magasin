package com.springproject.magasin.serviceinterfaces;

import com.springproject.magasin.models.Produit;

import java.util.List;

public interface ProduitServiceInter {
    List<Produit> getProduits();

    Produit saveProduit(Produit produit);

    Produit updateProduit(Produit produit);

    Produit getProduitById(Long id);

    String deleteProduit(Long id);
}
