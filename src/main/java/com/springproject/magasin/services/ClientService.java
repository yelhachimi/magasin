package com.springproject.magasin.services;

import com.springproject.magasin.models.Client;
import com.springproject.magasin.models.Commande;
import com.springproject.magasin.repositories.ClientRepository;
import com.springproject.magasin.repositories.CommandeRepository;
import com.springproject.magasin.serviceinterfaces.ClientServiceInter;

import java.util.List;
import java.util.Set;

public class ClientService implements ClientServiceInter {

    private ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository)
    {
        this.clientRepository = clientRepository;
    }

    @Override
    public List<Client> getClients()
    {
        return this.clientRepository.findAll();
    }

    @Override
    public Client saveClient(Client client)
    {
        return this.clientRepository.save(client);
    }

    @Override
    public Client getClientById(Long id)
    {
        return this.clientRepository.findById(id).get();
    }

    public Client updateClient(Client client)
    {
        return this.clientRepository.save(client);
    }

    public String deleteClient(Long id)
    {
        this.clientRepository.deleteById(id);
        return "Client supprimé.";
    }

    public Set<Commande> getCommandesClient(Long id)
    {
        Client client = this.clientRepository.findById(id).get();
        return client.getCommandes();
    }
}
