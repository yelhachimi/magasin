package com.springproject.magasin.services;

import com.springproject.magasin.models.Commande;
import com.springproject.magasin.repositories.CommandeRepository;
import com.springproject.magasin.serviceinterfaces.CommandeServiceInter;

import java.util.List;

public class CommandeService implements CommandeServiceInter {

    private CommandeRepository commandeRepository;

    public CommandeService(CommandeRepository commandeRepository)
    {
        this.commandeRepository = commandeRepository;
    }

    public List<Commande> getCommandes()
    {
        return this.commandeRepository.findAll();
    }

    public Commande saveCommande(Commande commande)
    {
        return this.commandeRepository.save(commande);
    }

    public Commande getCommandeById(Long id)
    {
        return this.commandeRepository.findById(id).get();
    }

    public Commande updateCommande(Commande commande)
    {
        return this.commandeRepository.save(commande);
    }

    public String deleteCommande(Long id)
    {
        this.commandeRepository.deleteById(id);
        return "Commande supprimée.";
    }
}
