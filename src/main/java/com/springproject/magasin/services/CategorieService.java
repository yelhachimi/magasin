package com.springproject.magasin.services;

import com.springproject.magasin.models.Categorie;
import com.springproject.magasin.repositories.CategorieRepository;
import com.springproject.magasin.serviceinterfaces.CategorieServiceInter;

import java.util.List;

public class CategorieService implements CategorieServiceInter {

    private CategorieRepository categorieRepository;

    public CategorieService(CategorieRepository categorieRepository)
    {
        this.categorieRepository = categorieRepository;
    }

    @Override
    public List<Categorie> getCategories()
    {
        return this.categorieRepository.findAll();
    }

    @Override
    public Categorie saveCategorie(Categorie categorie)
    {
        return this.categorieRepository.save(categorie);
    }

    public Categorie getCategorieById(Long id)
    {
        return this.categorieRepository.findById(id).get();
    }

    public Categorie updateCategorie(Categorie categorie)
    {
        return this.categorieRepository.save(categorie);
    }

    public String deleteCategorie(Long id)
    {
        this.categorieRepository.deleteById(id);
        return "Categorie supprimée.";
    }
}
