package com.springproject.magasin.services;

import com.springproject.magasin.models.Produit;
import com.springproject.magasin.repositories.ProduitRepository;
import com.springproject.magasin.serviceinterfaces.ProduitServiceInter;

import java.util.List;

public class ProduitService implements ProduitServiceInter {

    private ProduitRepository produitRepository;

    public ProduitService(ProduitRepository produitRepository)
    {
        this.produitRepository = produitRepository;
    }

    @Override
    public List<Produit> getProduits()
    {
        return this.produitRepository.findAll();
    }

    @Override
    public Produit saveProduit(Produit produit)
    {
        return this.produitRepository.save(produit);
    }

    public Produit getProduitById(Long id)
    {
        return this.produitRepository.findById(id).get();
    }

    public Produit updateProduit(Produit produit)
    {
        return this.produitRepository.save(produit);
    }

    public String deleteProduit(Long id)
    {
        this.produitRepository.deleteById(id);
        return "Produit supprimé.";
    }
}
