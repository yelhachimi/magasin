package com.springproject.magasin.controllers;

import com.springproject.magasin.models.Produit;
import com.springproject.magasin.services.ProduitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/produits")
public class ProduitController {

    Logger LOGGER = LoggerFactory.getLogger(ProduitController.class);

    private ProduitService produitService;

    public ProduitController(ProduitService produitService)
    {
        this.produitService = produitService;
    }

    @GetMapping
    public List<Produit> getProduits()
    {
        LOGGER.info("Info produit log");
        LOGGER.warn("Warn produit log");
        return this.produitService.getProduits();
    }

    @PostMapping
    public Produit saveProduit(@RequestBody Produit produit)
    {
        return this.produitService.saveProduit(produit);
    }

    @PutMapping
    public Produit updateProduit(@RequestBody Produit produit)
    {
        return this.produitService.updateProduit(produit);
    }

    @GetMapping("{id}")
    public Produit getProduitById(@PathVariable Long id)
    {
        return this.produitService.getProduitById(id);
    }

    @DeleteMapping("{id}")
    public String deleteProduit(@PathVariable Long id)
    {
        return this.produitService.deleteProduit(id);
    }
}
