package com.springproject.magasin.controllers;

import com.springproject.magasin.models.Commande;
import com.springproject.magasin.services.CommandeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/commandes")
public class CommandeController {

    Logger LOGGER = LoggerFactory.getLogger(CommandeController.class);

    private CommandeService commandeService;

    public CommandeController(CommandeService commandeService)
    {
        this.commandeService = commandeService;
    }

    @GetMapping
    public List<Commande> getCommandes()
    {
        LOGGER.info("Info commande log");
        LOGGER.warn("Warn commande log");
        return this.commandeService.getCommandes();
    }

    @PostMapping
    public Commande saveCommande(@RequestBody Commande commande)
    {
        return this.commandeService.saveCommande(commande);
    }

    @PutMapping
    public Commande updateCommande(@RequestBody Commande commande)
    {
        return this.commandeService.updateCommande(commande);
    }

    @GetMapping("{id}")
    public Commande getCommandeById(@PathVariable Long id)
    {
        return this.commandeService.getCommandeById(id);
    }

    @DeleteMapping("{id}")
    public String deleteCommande(@PathVariable Long id)
    {
        return this.commandeService.deleteCommande(id);
    }

}
