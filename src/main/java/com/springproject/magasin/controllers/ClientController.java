package com.springproject.magasin.controllers;

import com.springproject.magasin.models.Client;
import com.springproject.magasin.models.Commande;
import com.springproject.magasin.services.ClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/clients")
public class ClientController {

    Logger LOGGER = LoggerFactory.getLogger(ClientController.class);

    private ClientService clientService;

    public ClientController(ClientService clientService)
    {
        this.clientService = clientService;
    }

    @PostMapping
    public Client saveClient(@RequestBody Client client)
    {
        return this.clientService.saveClient(client);
    }

    @GetMapping
    public List<Client> getClients()
    {
        LOGGER.info("Info client log");
        LOGGER.warn("Warn client log");
        return this.clientService.getClients();
    }

    @GetMapping("{id}")
    public Client getClientById(@PathVariable Long id)
    {
        return this.clientService.getClientById(id);
    }

    @PutMapping
    public Client updateClient(@RequestBody Client client)
    {
        return this.clientService.updateClient(client);
    }

    @DeleteMapping("{id}")
    public String deleteClient(@PathVariable Long id)
    {
        return this.clientService.deleteClient(id);
    }

    @GetMapping("{id}/commandes")
    public Set<Commande> getCommandesClient(@PathVariable Long id)
    {
        return this.clientService.getCommandesClient(id);
    }
}
