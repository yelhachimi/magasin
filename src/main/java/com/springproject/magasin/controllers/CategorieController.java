package com.springproject.magasin.controllers;

import com.springproject.magasin.models.Categorie;
import com.springproject.magasin.services.CategorieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/categories")
public class CategorieController {

    Logger LOGGER = LoggerFactory.getLogger(CategorieController.class);

    private CategorieService categorieService;

    public CategorieController(CategorieService categorieService)
    {
        this.categorieService = categorieService;
    }

    @PostMapping
    public Categorie saveCategorie(@RequestBody Categorie categorie)
    {
        return this.categorieService.saveCategorie(categorie);
    }

    @GetMapping
    public List<Categorie> getCategories()
    {
        LOGGER.info("Info categorie log");
        LOGGER.warn("Warn categorie log");
        return this.categorieService.getCategories();
    }

    @GetMapping("{id}")
    public Categorie getCategorieById(@PathVariable Long id)
    {
        return this.categorieService.getCategorieById(id);
    }

    @PutMapping
    public Categorie updateCategorie(@RequestBody Categorie categorie)
    {
        return this.categorieService.updateCategorie(categorie);
    }

    @DeleteMapping("{id}")
    public String deleteCategorie(@PathVariable Long id)
    {
        return this.categorieService.deleteCategorie(id);
    }
}
