package com.springproject.magasin.config;

import com.springproject.magasin.repositories.CategorieRepository;
import com.springproject.magasin.repositories.ClientRepository;
import com.springproject.magasin.repositories.CommandeRepository;
import com.springproject.magasin.repositories.ProduitRepository;
import com.springproject.magasin.services.CategorieService;
import com.springproject.magasin.services.ClientService;
import com.springproject.magasin.services.CommandeService;
import com.springproject.magasin.services.ProduitService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MagasinConfig {

    @Bean
    public CategorieService categorieService(CategorieRepository categorieRepository)
    {
        return new CategorieService(categorieRepository);
    }

    @Bean
    public ProduitService produitService(ProduitRepository produitRepository)
    {
        return new ProduitService(produitRepository);
    }

    @Bean
    public ClientService clientService(ClientRepository clientRepository)
    {
        return new ClientService(clientRepository);
    }

    @Bean
    CommandeService commandeService(CommandeRepository commandeRepository)
    {
        return new CommandeService(commandeRepository);
    }
}
